return {
    on = {
        devices = {
            'ThermostatSP', 'BedRoomTH', 'LivingRoomTH', 'pixel', 'oneplus',
            'pc-new', 'pc-old'
        }
    },
    logging = {level = domoticz.LOG_INFO},
    execute = function(domoticz, triggeredDevice)

        if (domoticz.devices('pixel').state == 'Off') and
            (domoticz.devices('oneplus').state == 'Off') and
            (domoticz.devices('pc-new').state == 'Off') and
            (domoticz.devices('pc-old').state == 'Off') then
            domoticz.devices('Heater').switchOff().checkFirst()
        else
            local currentTemperature = 0

            local minutesSinceMidnight = domoticz.time.minutesSinceMidnight
            if (minutesSinceMidnight > 8 * 60) and
                (minutesSinceMidnight < 20 * 60) then
                currentTemperature = domoticz.devices('LivingRoomTH')
                                         .temperature
            else
                currentTemperature = domoticz.devices('BedRoomTH').temperature
            end

            if (domoticz.devices('pc-old').state == 'On') and
                (domoticz.devices('LivingRoomTH').temperature <
                    currentTemperature) then
                currentTemperature = domoticz.devices('LivingRoomTH')
                                         .temperature
            end

            if (domoticz.devices('pc-new').state == 'On') and
                (domoticz.devices('BedRoomTH').temperature < currentTemperature) then
                currentTemperature = domoticz.devices('BedRoomTH').temperature
            end

            if (currentTemperature < domoticz.devices('ThermostatSP').setPoint) then
                domoticz.devices('Heater').switchOn().checkFirst()
            elseif (currentTemperature >
                domoticz.devices('ThermostatSP').setPoint) then
                domoticz.devices('Heater').switchOff().checkFirst()
            end
        end
    end
}
