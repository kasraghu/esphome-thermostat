# Domoticz Esphome Xiaomi Mijia LYWSD03MMC

## ESPHome
[thermostat.yaml](./thermostat.yaml)
### Python
1. ```python3 -m venv .venv```
2. ```source ./.venv/bin/activate```
3. ```pip install esphome```
4. ```esphome run thermostat.yaml```

## Xiaomi Mijia LYWSD03MMC

https://github.com/pvvx/ATC_MiThermometer

## Domoticz plugin
[domoticz-plugin.py](./domoticz-plugin.py)

### Python
1. ```python3 -m venv .venv```
2. ```source ./.venv/bin/activate```
3. ```pip install bleak requests```

### Systemd
* Check the filepaths in domoticz-mijia.service
* Move systemd files to (on Debian based system)```/lib/systemd/system```
  1. ```systemctl daemon-reload```
  2. ```systemctl enable --now domoticz-mijia.timer```

## Domoticz DzVents script
[thermostat.lua](./thermostat.lua)