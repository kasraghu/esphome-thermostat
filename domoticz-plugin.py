import asyncio
from bleak import BleakScanner
from bleak.backends.device import BLEDevice
from bleak.backends.scanner import AdvertisementData
import struct
import requests

timeout_limit = 10 # seconds
addresses = ("A4:C1:38:59:8E:50", "A4:C1:38:DD:90:B0")
indexes = ("1", "2")
adv_data_collection = []

def humidity_status(temperature, humidity):
    if humidity <= 30:
        return 2
    elif humidity >= 70:
        return 3
    elif humidity >= 35 and humidity <= 65 and temperature >= 20 and temperature <= 24:
        return 1
    else:
        return 0

def callback_handler(device: BLEDevice, advertisement_data: AdvertisementData):
    if device.address in addresses:
        data = advertisement_data.service_data.get('0000181a-0000-1000-8000-00805f9b34fb')
        if data is not None:
            adv_data_collection.append((data, device))
            
async def run():
    scanner = BleakScanner(filters={"DuplicateData": False})
    scanner.register_detection_callback(callback_handler)
    await scanner.start()
    seconds_count = 0
    while seconds_count < timeout_limit:
        await asyncio.sleep(1.0)
        seconds_count = seconds_count + 1
    await scanner.stop()
    for adv_data in adv_data_collection:
        data = adv_data[0]
        device = adv_data[1]
        data_len = len(data)
        # https://github.com/pvvx/pvvx.github.io/blob/master/ATC_MiThermometer/TelinkMiFlasher.html#L289
        if(data_len >= 15):
            temperature, humidity, bat_voltage, battery, count, flags = struct.unpack('<hhHBBB', data[6:])
            temperature = temperature/100.0
            humidity = humidity/100.0
        if(data_len == 13):
            temperature, humidity, battery, bat_voltage, count = struct.unpack('>hBBHB', data[6:])
            temperature = temperature/10.0
        status = humidity_status(temperature, humidity)
        index = indexes[addresses.index(device.address)]
        get_request = 'http://127.0.0.1:8080/json.htm?type=command&param=udevice&idx='+str(index)+'&nvalue=0&svalue='+str(temperature)+';'+str(humidity)+';'+str(status)+'&battery='+str(battery)
        # print(get_request)
        print(requests.get(get_request)) # keep the print or else the function may return before completing the request

loop = asyncio.get_event_loop()
loop.run_until_complete(run())

