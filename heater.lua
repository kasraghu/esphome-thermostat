return {
    on = {timer = {'every minute'}, devices = {'Heater'}},
    -- logging = {level = domoticz.LOG_INFO},
    execute = function(domoticz, triggeredItem)
        if (domoticz.devices('Heater').state == 'On') then
            domoticz.openURL({
                url = 'http://192.168.0.6/switch/trigger/turn_on',
                method = 'POST'
            })
        end
    end
}
